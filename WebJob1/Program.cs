﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.ServiceBus;

namespace WebJob1
{
    // To learn more about Microsoft Azure WebJobs SDK, please see https://go.microsoft.com/fwlink/?LinkID=320976
    class Program
    {
        // Please set the following connection strings in app.config for this WebJob to run:
        // AzureWebJobsDashboard and AzureWebJobsStorage
        static void Main()
        {
            var config = new JobHostConfiguration();

            if (config.IsDevelopment)
            {
                config.UseDevelopmentSettings();
            }

            var eventHubConfig = new EventHubConfiguration();
            eventHubConfig.AddReceiver("outtest", "Endpoint=sb://metricpb-eventhub.servicebus.windows.net/;SharedAccessKeyName=outtestkeys;SharedAccessKey=xkMBTVyf9Us9ct32VCpmRIzU4QS8p0cmymPRK1c3/qY=");
            eventHubConfig.AddReceiver("from-mobile", "Endpoint=sb://metricpb-eventhub.servicebus.windows.net/;SharedAccessKeyName=from-mobile;SharedAccessKey=7bNWskqP4Z9DKUC3NutKe7t2hyE6nBl9VGGiU7fHpbo=");

            config.UseEventHub(eventHubConfig);

            var host = new JobHost(config);
            // The following code ensures that the WebJob will be running continuously
            host.RunAndBlock();
        }
    }
}
