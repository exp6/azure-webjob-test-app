﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.ServiceBus;
using System.Runtime.Serialization.Json;
using Newtonsoft.Json;
using PubnubApi;
using Microsoft.Azure.Devices;

namespace WebJob1
{
    public class Functions
    {
        // This function will get triggered/executed when a new message is written 
        // on an Azure Queue called queue.
        //public static void ProcessQueueMessage([QueueTrigger("queue")] string message, TextWriter log)
        //{
        //    log.WriteLine(message);
        //}

        static private Pubnub pubnub;

        static ServiceClient serviceClient;
        static string connectionString = "HostName=iot-lab-hub.azure-devices.net;SharedAccessKeyName=iothubowner;SharedAccessKey=EUUDF3PkAm1qyNsxu9glVDEeBBkLmVnw5y3Wn66jCzE=";

        public static void ProcessQueueMessage([EventHubTrigger("outtest")] string eventData)
        {
            //Console.WriteLine(eventData);
            dynamic result = JsonConvert.DeserializeObject(eventData);
            string device = result["deviceId"];
            dynamic telemetryData = result["data"];
            double oxygen = telemetryData["oxygen"];
            string clientId = telemetryData["clientId"];
            int woodType = telemetryData["woodType"];
            
            using (var db = new TestDbContext())
            {
                List<TunnelComponent> componentsList = db.TunnelComponent.ToList();
                int oxygenValue = (int)Math.Round(oxygen, 0);

                TunnelComponent component = null;

                foreach (TunnelComponent tc in componentsList)
                {
                    if((int)tc.o2_range == oxygenValue)
                    {
                        component = tc;
                        break;
                    }
                }

                if(component != null)
                {
                    double oxygenMpFix = getOxygenMpFix(component);
                    double oxygenCoFix = getOxygenCoFix(component);
                    double airExcessPercentage = getAirExcessPercentage(component);
                    int fireStatus = getFireStatus(component);

                    PNConfiguration config = new PNConfiguration();
                    config.SubscribeKey = "sub-c-c7207e20-513f-11e7-a368-0619f8945a4f";
                    config.PublishKey = "pub-c-a3c37225-35ff-480d-9375-fc3722d2116c";

                    pubnub = new Pubnub(config);

                    if (airExcessPercentage > 50) sendC2DMessage(device);

                    String str = Math.Round(airExcessPercentage, 1).ToString();

                    pubnub.Publish()
                    .Channel("awesomeChannel")
                    .Message(str)
                    .Async(new PNPublishResultExt((publishResult, publishStatus) => {
                        // Check whether request successfully completed or not.
                        if (!publishStatus.Error)
                        {
                            Console.WriteLine("Message successfully published to specified channel");
                            // Message successfully published to specified channel.
                        }
                        else
                        {
                            Console.WriteLine("Request processing failed: " + publishStatus.Category.ToString());
                            
                            // Request processing failed.

                            // Handle message publish error. Check 'Category' property to find out possible issue
                            // because of which request did fail.
                        }
                    }));
                }
            }
        }

        public static void ListenFromMobileApp([EventHubTrigger("from-mobile")] string eventData)
        {
            Console.WriteLine(eventData);
            dynamic result = JsonConvert.DeserializeObject(eventData);
        }

        private static void sendC2DMessage(string deviceId)
        {
            Console.WriteLine("Send Cloud-to-Device message to "+ deviceId + "\n");
            serviceClient = ServiceClient.CreateFromConnectionString(connectionString);
            ReceiveFeedbackAsync(deviceId);
            SendCloudToDeviceMessageAsync(deviceId).Wait();
        }

        private async static Task SendCloudToDeviceMessageAsync(string deviceId)
        {
            var commandMessage = new Message(Encoding.ASCII.GetBytes("More than 50% of air excess!!"));
            await serviceClient.SendAsync(deviceId, commandMessage);
        }

        private async static void ReceiveFeedbackAsync(string deviceId)
        {
            var feedbackReceiver = serviceClient.GetFeedbackReceiver();

            Console.WriteLine("\nReceiving c2d feedback from device: " + deviceId);
            while (true)
            {
                var feedbackBatch = await feedbackReceiver.ReceiveAsync();
                if (feedbackBatch == null) continue;

                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("Received feedback: {0}", string.Join(", ", feedbackBatch.Records.Select(f => f.StatusCode)));
                Console.ResetColor();

                await feedbackReceiver.CompleteAsync(feedbackBatch);
            }
        }

        private static double getOxygenMpFix(TunnelComponent component)
        {
            double mp = component.mp * ((21-6)/(21-component.o2_range));
            double mpFiltered = component.mp_filter * ((21 - 6) / (21 - component.o2_range));

            double rate = (mp - mpFiltered) / (mp + mpFiltered);
            double perc = rate * 100;

            return perc;
        }

        private static double getOxygenCoFix(TunnelComponent component)
        {
            double co = component.co * ((21 - 6) / (21 - component.o2_range));
            double coFiltered = component.co_filter * ((21 - 6) / (21 - component.o2_range));

            double rate = (co - coFiltered) / (co + coFiltered);
            double perc = rate * 100;

            return perc;
        }

        private static double getAirExcessPercentage(TunnelComponent component)
        {
            double n2BalanceFire = getN2BalanceFire(component);
            double finalBalanceValue = (n2BalanceFire > 78 ? 78 : n2BalanceFire);

            double res = 100 * ((component.o2_range-(0.5*(component.co_filter/10000))) / ((0.264* finalBalanceValue) -(component.o2_range-(0.5*(component.co_filter/ 10000)))));

            return res;
        }

        private static double getN2BalanceFire(TunnelComponent component)
        {
            return (100 - component.co2_filter - component.o2_range - (component.co_filter / 10000) - (component.so2_filter / 10000) - (component.nox_filter / 10000));
        }

        private static int getFireStatus(TunnelComponent component)
        {
            double foRange1 = 1;
            double foRange2 = 1.2;
            double qualityControlFactor = getQualityControlFactor(component);
            return (qualityControlFactor < foRange1 || qualityControlFactor > foRange2 ? 1 : 0);
        }

        private static double getQualityControlFactor(TunnelComponent component)
        {
            return ((21-component.o2_range)/component.co2);
        } 
    }
}
