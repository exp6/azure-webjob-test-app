﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebJob1
{
    public class TunnelComponent
    {
        public Guid id { get; set; }
        public double o2_range { get; set; }
        public double nox { get; set; }
        public double so2 { get; set; }
        public double mp { get; set; }
        public double co { get; set; }
        public double co2 { get; set; }
        public double hum { get; set; }
        public double tem { get; set; }
        public double flow { get; set; }
        public double nox_filter { get; set; }
        public double so2_filter { get; set; }
        public double mp_filter { get; set; }
        public double co_filter { get; set; }
        public double co2_filter { get; set; }
        public double hum_filter { get; set; }
        public double tem_filter { get; set; }
        public double flow_filter { get; set; }
    }
}
